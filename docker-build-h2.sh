#!/bin/bash
PLATFORM=h2
VERSION=5.0.1-01
docker build -f Dockerfile --build-arg BSKP_PLATFORM=${PLATFORM} -t boodskapiotplatform/platform-${PLATFORM}:latest -t boodskapiotplatform/platform-${PLATFORM}:${VERSION} ./target/${PLATFORM}
