package io.boodskap.iot.api;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaMSF4JServerCodegen", date = "2022-01-26T11:51:16.025588+05:30[Asia/Kolkata]")
public class NotFoundException extends ApiException {

	private static final long serialVersionUID = 7629912050721450810L;

	public NotFoundException (int code, String msg) {
        super(code, msg);
    }
}
