package io.boodskap.iot.api;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FatAPIGenerator {

	public static void main(String[] args) throws IOException {
		
		File folder = new File("src/main/resources");
		File[] yamls = folder.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return (f.isFile() && f.getName().startsWith("api-") && f.getName().endsWith(".yaml"));
			}
		});
		
		if(null != yamls) {
			
			System.out.format("%d YAMLS found\n", yamls.length);
			List<String> allLines = new ArrayList<>();
			
			for(File yaml : yamls) {
				System.out.format("---%s---\n", yaml.getName());
				List<String> lines = FileUtils.readLines(yaml);
				lines.subList(0, 10).clear();
				allLines.addAll(lines);
			}
			
			File api = new File(folder, "api.yaml");
			File header = new File(folder, "combined.yaml");
			File footer = new File(folder, "definitions.yaml");
			
			List<String> headers = FileUtils.readLines(header);
			List<String> footers = FileUtils.readLines(footer);
			
			FileUtils.writeLines(api, headers);
			FileUtils.writeLines(api, allLines, true);
			FileUtils.writeLines(api, footers, true);
			
		}
	}
}
