  /mspec/upsert:
    post:
      tags:
        - Message Specification
      summary: Create or Update Message Specification
      operationId: upsertMessageSpec
      requestBody:
        description: CIMessageSpecification JSON object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CIMessageSpecification'
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/get/{domainKey}/{specId}:
    get:
      tags:
        - Message Specification
      operationId: getMessageSpec
      summary: Retrieve Message Specification
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CIMessageSpecification'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/list/{domainKey}/{page}/{pageSize}:
    get:
      tags:
        - Message Specification
      operationId: listMessageSpecs
      summary: List Message Spcifications
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
        - in: query
          name: next
          required: false
          schema:
            type: boolean
        - in: query
          name: specId
          required: false
          description: required if **next** is true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CIMessageSpecification'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/total:
    get:
      tags:
        - Message Specification
      operationId: countAllMessageSpecs
      summary: Count Total Message Specifications
      description: Admin access required
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Count'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/count/{domainKey}:
    get:
      tags:
        - Message Specification
      operationId: countDomainMessageSpecs
      summary: Count Domain Message Specifications
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Count'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/search/{domainKey}/{page}/{pageSize}:
    post:
      tags:
        - Message Specification
      operationId: searchMessageSpecifications
      summary: Search Message Specifications
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
      requestBody:
        description: Search Query (Storage specific query)
        required: true
        content:
          text/plain:
            schema:
              type: string
          application/json:
            schema:
              type: object
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CIMessageSpecification'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/delete/{domainKey}:
    delete:
      tags:
        - Message Specification
      operationId: deleteDomainMessageSpecs
      summary: Delete Domain Message Specifications
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/delete/{domainKey}/{specId}:
    delete:
      tags:
        - Message Specification
      operationId: deleteMessageSpec
      summary: Delete Message Specification
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/clear:
    delete:
      tags:
        - Message Specification
      operationId: deleteAllMessageSpecs
      summary: Delete All Message Specifications
      description: Admin access required
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

  /mspec/field/can/add:
    get:
      tags:
        - Message Specification
      operationId: canAddMspecField
      summary: Can add new field
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/field/can/drop:
    get:
      tags:
        - Message Specification
      operationId: canDropMspecField
      summary: Can delete a field
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/field/can/modify:
    get:
      tags:
        - Message Specification
      operationId: canModifyMspecField
      summary: Can modify a field
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/field/add/{domainKey}/{specId}:
    post:
      tags:
        - Message Specification
      operationId: addMspecField
      summary: Add new field
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      requestBody:
        description: CIMessageField JSON object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CIMessageField'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/field/drop/{domainKey}/{specId}/{fname}:
    delete:
      tags:
        - Message Specification
      operationId: dropMspecField
      summary: Delete a field
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
        - in: path
          name: fname
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /mspec/field/modify/{domainKey}/{specId}:
    post:
      tags:
        - Message Specification
      operationId: modifyMspecField
      summary: Modify a field
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      requestBody:
        description: CIMessageField JSON object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CIMessageField'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BoolResult'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
