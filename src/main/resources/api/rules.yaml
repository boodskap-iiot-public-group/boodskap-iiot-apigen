openapi: 3.0.0
info:
  title: Boodskap IoT Platform API
  description: Boodskap IoT Platform API
  version: 5.0.0-00
servers:
  - url: http://boodskap.xyz/api
    description: Local server
  - url: https://platform5.boodskap.io/api
    description: Main testing server
security:
  - ApiTokenAuth: []
  - ApiDomainAuth: []
  - ApiKeyAuth: []
  - ApiLoginID: []
  - ApiLoginPswd: []
paths:
  /rule/upsert:
    post:
      tags:
        - Rule
      summary: Create or Update Rule
      operationId: upsertRule
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CIDomainRule'
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/get/{domainKey}/{ruleType}:
    get:
      tags:
        - Rule
      operationId: getRule
      summary: Retrieve Rule
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: ruleType
          required: true
          schema:
            type: string
            enum:
              - DOMAIN
              - NAMED
              - SCHEDULED
              - MESSAGE
        - in: query
          name: name
          required: false
          description: Required if type is NAMED 
          schema:
            type: string
        - in: query
          name: ruleId
          required: false
          description: Required if type is SCHEDULED 
          schema:
            type: string
        - in: query
          name: specId
          required: false
          description: Required if type is MESSAGE 
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CIDomainRule'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/list/{page}/{pageSize}:
    get:
      tags:
        - Rule
      operationId: listRules
      summary: List Rules
      description: Admin access required
      parameters:
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CIDomainRule'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/total:
    get:
      tags:
        - Rule
      operationId: countAllRules
      summary: Count total domains
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Count'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/search/{page}/{pageSize}:
    post:
      tags:
        - Rule
      operationId: searchRules
      summary: Search domains
      description: Admin access required
      parameters:
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
      requestBody:
        description: Search query string
        required: true
        content:
          text/plain:
            schema:
              type: string
          application/json:
            schema:
              type: object
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CIDomainRule'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/delete/{domainKey}:
    delete:
      tags:
        - Rule
      operationId: deleteRule
      summary: Delete Rule
      description: Admin access required
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /rule/clear:
    delete:
      tags:
        - Rule
      operationId: deleteAllRules
      summary: Delete All Rules
      description: Admin access required
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'

components:
  securitySchemes:
    ApiTokenAuth:
      type: apiKey
      in: header
      name: X-ATOKEN
    ApiDomainAuth:
      type: apiKey
      in: header
      name: X-DOMKEY
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-APIKEY
    ApiLoginID:
      type: apiKey
      in: header
      name: X-USER
    ApiLoginPswd:
      type: apiKey
      in: header
      name: X-PASSWD
  responses:
    UnauthorizedError:
      description: API key is missing or invalid
      headers:
        WWW_Authenticate:
          schema:
            type: string
  schemas:
    BoolResult:
      type: object
      required:
        - value
      properties:
        value:
          type: boolean
    ApiError:
      type: object
      properties:
        message:
          type: string
        code:
          type: integer
          format: int32
        trace:
          type: string
      required:
        - message
    Success: 
      type: "object"
      properties: 
        code: 
          type: "string"
        message: 
          type: "string"
    IDResult:
      type: object
      required:
        - id
      properties:
        id:
          type: string
          format: uuid
    Count:
      type: object
      required:
        - total
      properties:
        total:
          type: integer
          format: int64
    CIStorageObject:
      type: object
      description: A representation IStorageObject interface
      discriminator:
        propertyName: storageObject
      properties:
        registeredStamp:
          type: string
          format: date-time
        updatedStamp:
          type: string
          format: date-time
        extraProperties:
          type: string
    CIEntity:
      description: A representation of IEntity interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIStorageObject'
      - type: object
        properties:
          createdBy:
            type: string
          updatedBy:
            type: string
          description:
            type: string
          entity:
            type: object
    CIModel:
      description: A representation of IModel interface
      discriminator:
        propertyName: model
      allOf:
      - $ref: '#/components/schemas/CIEntity'
      - type: object
        properties:
          name:
            type: string
    CIDomainContent:
      type: object
      description: A representation IDomainContent interface
      discriminator:
        propertyName: domainContent
      properties:
        domainKey:
          type: string
    CIDomainObject:
      description: A representation of IDomainObject interface
      discriminator:
        propertyName: domainObject
      allOf:
      - $ref: '#/components/schemas/CIModel'
      - type: object
        properties:
          domainKey:
            type: string
    CIContact:
      description: A representation of IContact interface
      discriminator:
        propertyName: contact
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        properties:
          email:
            type: string
          country:
            type: string
          state:
            type: string
          city:
            type: string
          address:
            type: string
          zipcode:
            type: string
          locale:
            type: string
          timeZone:
            type: string
          primaryPhone:
            type: string
    CIDomain:
      description: A representation of IDomain interface
      discriminator:
        propertyName: domain
      allOf:
      - $ref: '#/components/schemas/CIContact'
      - type: object
    CIPerson:
      description: A representation of IPerson interface
      discriminator:
        propertyName: person
      allOf:
      - $ref: '#/components/schemas/CIContact'
      - type: object
        properties:
          firstName:
            type: string
          lastName:
            type: string
          password:
            type: string
            format: password
          workHourStart:
            type: integer
          workHourEnd:
            type: integer
    CIUser:
      description: A representation of IUser interface
      discriminator:
        propertyName: user
      allOf:
      - $ref: '#/components/schemas/CIPerson'
      - type: object
        required:
          - userId
        properties:
          userId:
            type: string
    CIField:
      description: A representation of IField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIStorageObject'
      - type: object
        required:
          - name
          - dataType
        properties:
          name:
            type: string
          description:
            type: string
          dataType:
            type: string
            enum:
              - BOOLEAN
              - BYTE
              - CHAR
              - SHORT
              - INT
              - LONG
              - FLOAT
              - DOUBLE
              - STRING
              - UUID
              - BLOB
              - JSON
              - _boolean
              - _byte
              - _char
              - _short
              - _int
              - _long
              - _float
              - _double
              - _blob
    CIMessageField:
      description: A representation of IMessageField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIField'
      - type: object
        properties:
          indexed:
            type: boolean
          fullTextIndexed:
            type: boolean
    CIMessageSpecification:
      description: A representation of IMessageSpecification interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - specId
          - name
        properties:
          specId:
            type: string
          fields:
            type: array
            items:
              $ref: '#/components/schemas/CIMessageField'
    CIRecordField:
      description: A representation of IRecordField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIField'
      - type: object
        properties:
          indexed:
            type: boolean
          fullTextIndexed:
            type: boolean
    CIRecordSpecification:
      description: A representation of IRecordSpecification interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - specId
          - name
        properties:
          specId:
            type: string
          fields:
            type: array
            items:
              $ref: '#/components/schemas/CIRecordField'
    CIDomainRule:
      description: A representation of IRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - code
        properties:
          code:
            type: string
          contexts:
            type: array
            items:
              type: string
          plugins:
            type: array
            items:
              type: string
          compilable:
            type: boolean
          loader:
            type: string
          globalLoader:
            type: string
          language:
            type: string
            enum:
              - GROOVY
              - JAVA_SCRIPT
    CIDomainRule:
      description: A representation of IDomainRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainRule'
      - type: object
    CINamedRule:
      description: A representation of INamedRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainRule'
      - type: object
        required:
          - name
    CIMessageRule:
      description: A representation of IMessageRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainRule'
      - type: object
        required:
          - specId
        properties:
          specId:
            type: string
    CIScheduledRule:
      description: A representation of IScheduledRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainRule'
      - type: object
        required:
          - ruleId
          - pattern
        properties:
          specId:
            type: string
          pattern:
            type: string
    CIBinaryRule:
      description: A representation of IBinaryRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainRule'
      - type: object
        required:
          - type
        properties:
          type:
            type: string
