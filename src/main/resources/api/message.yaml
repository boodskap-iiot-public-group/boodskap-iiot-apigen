openapi: 3.0.0
info:
  title: Boodskap IoT Platform API
  description: Boodskap IoT Platform API
  version: 5.0.2-00
servers:
  - url: http://boodskap.xyz/api
    description: Local server
  - url: https://v5.boodskap.io/api
    description: Main testing server
  - url: http://localhost:18080
    description: Local Dev Server
security:
  - ApiTokenAuth: []
  - ApiDomainAuth: []
  - ApiDKeyAuth: []
  - ApiLoginID: []
  - ApiLoginPswd: []
paths:
  /message/get/{domainKey}/{specId}/{messageId}:
    get:
      tags:
        - Message
      operationId: getMessage
      summary: Retrieve Message
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
        - in: path
          name: messageId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
               type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/list/{domainKey}/{specId}/{page}/{pageSize}:
    get:
      tags:
        - Message
      operationId: listMessages
      summary: List Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
        - in: query
          name: next
          required: false
          schema:
            type: boolean
        - in: query
          name: messageId
          required: false
          description: required if **next** is true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/count/{domainKey}/{specId}:
    get:
      tags:
        - Message
      operationId: countDomainMessages
      summary: Count Domain Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Count'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/search/{domainKey}/{specId}/{page}/{pageSize}:
    post:
      tags:
        - Message
      operationId: searchMessages
      summary: Search Devices
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
        - in: path
          name: page
          required: true
          schema:
            type: integer
        - in: path
          name: pageSize
          required: true
          schema:
            type: integer
      requestBody:
        description: Search Query (Storage specific query)
        required: true
        content:
          text/plain:
            schema:
              type: string
          application/json:
            schema:
              type: object
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CIDevice'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/delete/{domainKey}/{specId}/{messageId}:
    delete:
      tags:
        - Message
      operationId: deleteMessage
      summary: Delete Message
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
        - in: path
          name: messageId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/delete/{domainKey}/{specId}:
    delete:
      tags:
        - Message
      operationId: deleteDomainMessages
      summary: Delete All Domain Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/select/query/{domainKey}/{specId}:
    post:
      tags:
        - Message
      operationId: selectMessagesByQuery
      summary: Select Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      requestBody:
        description: Custom Query Object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CustomQuery'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/delete/query/{domainKey}/{specId}:
    post:
      tags:
        - Message
      operationId: deleteMessagesByQuery
      summary: Delete by Query Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      requestBody:
        description: Delete Query (Storage specific query)
        required: true
        content:
          text/plain:
            schema:
              type: string
          application/json:
            schema:
              type: object
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
  /message/update/query/{domainKey}/{specId}:
    post:
      tags:
        - Message
      operationId: updateMessagesByQuery
      summary: Update Messages
      parameters:
        - in: path
          name: domainKey
          required: true
          schema:
            type: string
        - in: path
          name: specId
          required: true
          schema:
            type: string
      requestBody:
        description: Custom Query Object
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CustomQuery'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: object
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiError'
components:
  securitySchemes:
    ApiTokenAuth:
      type: apiKey
      in: header
      name: X-ATOKEN
    ApiDomainAuth:
      type: apiKey
      in: header
      name: X-DOMKEY
    ApiDKeyAuth:
      type: apiKey
      in: header
      name: X-APIKEY
    ApiLoginID:
      type: apiKey
      in: header
      name: X-USER
    ApiLoginPswd:
      type: apiKey
      in: header
      name: X-PASSWD
    ApiTrace:
      type: apiKey
      in: header
      name: X-TRACE
  responses:
    UnauthorizedError:
      description: API key is missing or invalid
      headers:
        WWW_Authenticate:
          schema:
            type: string
  schemas:
    BoodskapConfig:
      type: object
      properties:
        storageType:
          type: string
        dynamicStorageType:
          type: string
        rawStorageType:
          type: string
        cacheType:
          type: string
        gridType:
          type: string
        queueType:
          type: string
        version:
          type: string
    BoolResult:
      type: object
      required:
        - value
      properties:
        value:
          type: boolean
    ApiError:
      type: object
      properties:
        message:
          type: string
        code:
          type: integer
          format: int32
        trace:
          type: string
      required:
        - message
    Success: 
      type: "object"
      properties: 
        code: 
          type: "string"
        message: 
          type: "string"
    IDResult:
      type: object
      required:
        - id
      properties:
        id:
          type: string
          format: uuid
    Count:
      type: object
      required:
        - total
      properties:
        total:
          type: integer
          format: int64
    CIStorageObject:
      type: object
      description: A representation IStorageObject interface
      discriminator:
        propertyName: storageObject
      properties:
        registeredStamp:
          type: string
          format: date-time
        updatedStamp:
          type: string
          format: date-time
        extraProperties:
          type: string
    CIEntity:
      description: A representation of IEntity interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIStorageObject'
      - type: object
        properties:
          createdBy:
            type: string
          updatedBy:
            type: string
          description:
            type: string
          entity:
            type: object
    CIModel:
      description: A representation of IModel interface
      discriminator:
        propertyName: model
      allOf:
      - $ref: '#/components/schemas/CIEntity'
      - type: object
        properties:
          name:
            type: string
    CIDomainContent:
      type: object
      description: A representation IDomainContent interface
      discriminator:
        propertyName: domainContent
      properties:
        domainKey:
          type: string
    CIDomainObject:
      description: A representation of IDomainObject interface
      discriminator:
        propertyName: domainObject
      allOf:
      - $ref: '#/components/schemas/CIModel'
      - type: object
        properties:
          domainKey:
            type: string
    CIContact:
      description: A representation of IContact interface
      discriminator:
        propertyName: contact
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        properties:
          email:
            type: string
          country:
            type: string
          state:
            type: string
          city:
            type: string
          address:
            type: string
          zipcode:
            type: string
          locale:
            type: string
          timeZone:
            type: string
          primaryPhone:
            type: string
    CIDomain:
      description: A representation of IDomain interface
      discriminator:
        propertyName: domain
      allOf:
      - $ref: '#/components/schemas/CIContact'
      - type: object
    CIPerson:
      description: A representation of IPerson interface
      discriminator:
        propertyName: person
      allOf:
      - $ref: '#/components/schemas/CIContact'
      - type: object
        properties:
          firstName:
            type: string
          lastName:
            type: string
          password:
            type: string
            format: password
          workHourStart:
            type: integer
          workHourEnd:
            type: integer
    CIUser:
      description: A representation of IUser interface
      discriminator:
        propertyName: user
      allOf:
      - $ref: '#/components/schemas/CIPerson'
      - type: object
        required:
          - userId
        properties:
          userId:
            type: string
    CIField:
      description: A representation of IField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIStorageObject'
      - type: object
        required:
          - name
          - dataType
        properties:
          name:
            type: string
          description:
            type: string
          dataType:
            type: string
            enum:
              - BOOLEAN
              - BYTE
              - CHAR
              - SHORT
              - INT
              - LONG
              - FLOAT
              - DOUBLE
              - STRING
              - UUID
              - BLOB
              - JSON
              - _boolean
              - _byte
              - _char
              - _short
              - _int
              - _long
              - _float
              - _double
              - _blob
    CIMessageField:
      description: A representation of IMessageField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIField'
      - type: object
        properties:
          indexed:
            type: boolean
          fullTextIndexed:
            type: boolean
    CIMessageSpecification:
      description: A representation of IMessageSpecification interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - specId
          - name
        properties:
          specId:
            type: string
          fields:
            type: array
            items:
              $ref: '#/components/schemas/CIMessageField'
    CIRecordField:
      description: A representation of IRecordField interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIField'
      - type: object
        properties:
          indexed:
            type: boolean
          fullTextIndexed:
            type: boolean
    CIRecordSpecification:
      description: A representation of IRecordSpecification interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - specId
          - name
        properties:
          specId:
            type: string
          fields:
            type: array
            items:
              $ref: '#/components/schemas/CIRecordField'
    CIRule:
      description: A representation of IRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - code
        properties:
          code:
            type: string
          contexts:
            type: array
            items:
              type: string
          plugins:
            type: array
            items:
              type: string
          compilable:
            type: boolean
          loader:
            type: string
          globalLoader:
            type: string
          language:
            type: string
            enum:
              - GROOVY
              - JAVA_SCRIPT
    CIDomainRule:
      description: A representation of IDomainRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIRule'
      - type: object
    CINamedRule:
      description: A representation of INamedRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIRule'
      - type: object
        required:
          - name
    CIMessageRule:
      description: A representation of IMessageRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIRule'
      - type: object
        required:
          - specId
        properties:
          specId:
            type: string
    CIScheduledRule:
      description: A representation of IScheduledRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIRule'
      - type: object
        required:
          - ruleId
          - pattern
        properties:
          specId:
            type: string
          pattern:
            type: string
    CIBinaryRule:
      description: A representation of IBinaryRule interface
      discriminator:
        propertyName: entity
      allOf:
      - $ref: '#/components/schemas/CIRule'
      - type: object
        required:
          - type
        properties:
          type:
            type: string
    CILinkedDomain:
      required:
        - domainKey
      properties:
        domainKey:
          type: string
        name:
          type: string
        description:
          type: string
    CIPartOrg:
      required:
        - orgId
      properties:
        orgId:
          type: string
        name:
          type: string
        description:
          type: string
    LoginRequest:
      type: object
      required:
        - userId
        - password
      properties:
        userId:
          type: string
        password:
          type: string
          format: password
        domainKey:
          type: string
          description: Supply this if you want to login to a specific domain
    LoginResponse:
      required:
        - user
        - domain
        - token
      properties:
        user:
          $ref: '#/components/schemas/CIUser'
        domain:
          $ref: '#/components/schemas/CIDomain'
        token:
          type: string
        linkedDomains:
          type: array
          items:
            $ref: '#/components/schemas/CILinkedDomain'
        partDomains:
          type: array
          items:
            $ref: '#/components/schemas/CILinkedDomain'
        access:
          type: array
          items:
            type: string
        orgs:
          type: array
          items:
            $ref: '#/components/schemas/CIPartOrg'
        roles:
          type: array
          items:
            type: string
    CIAccessToken:
      required:
        - token
      properties:
        token:
          type: string
        external:
          type: boolean
        authType:
          type: string
        domainKey:
          type: string
        userId:
          type: string
        deviceId:
          type: string
        orgId:
          type: string
        expireIn:
          type: integer
          format: int64
    CIDevice:
      description: A representation of IDevice interface
      discriminator:
        propertyName: device
      allOf:
      - $ref: '#/components/schemas/CIDomainObject'
      - type: object
        required:
          - deviceId
        properties:
          deviceId:
            type: string
          modelId:
            type: string
          version:
            type: string
          password:
            type: string
            format: password
          assetId:
            type: string
          reportedIp:
            type: string
          reportedPort:
            type: integer
          nodeId:
            type: string
          nodeUid:
            type: string
          channel:
            type: string
    CustomQuery:
      type: object
      required:
        - what
        - how
      properties:
        what:
          type: string
        how:
          type: string
    PushResult:
      type: object
      required:
        - id
        - load
      properties:
        id:
          type: string
          format: uuid
        load:
          type: integer
