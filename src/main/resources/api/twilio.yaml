swagger: "2.0"
info: 
  version: "1.0.6"
  title: "Boodskap IoT API"
  description: "Boodskap IoT Platform API"
  termsOfService: "http://boodskap.io/terms/"
  contact: 
    name: "Boodskap Team"
  license: 
    name: "MIT"
host: "api.boodskap.io"
schemes: 
  - "http"
  - "https"
produces: 
  - "application/json"
paths: 
  '/twilio/voice/lookup/{vcid}':
    post:
      tags:
        - Lookup Twilio Voice TwMil
      summary: Lookup Twilio Voice TwMil
      operationId: lookupTwilioVoiceXML
      consumes:
        - "text/plain"
      produces:
        - "application/xml"
      parameters:
        - name: vcid
          in: path
          description: Voice Content ID, typically UUID
          required: true
          type: string
      responses:
        '200':
          description: successful operation
          schema: 
            type: string
        "417":
          description: "API Error"
          schema: 
            $ref: "#/definitions/ApiError"
definitions: 
  User:
    required:
      - email
    properties:
      email:
        type: string
      password:
        type: string
      firstName:
        type: string
      lastName:
        type: string
      country:
        type: string
      state:
        type: string
      city:
        type: string
      address:
        type: string
      zipcode:
        type: string
      primaryPhone:
        type: string
      locale:
        type: string
      timezone:
        type: string
      workStart:
        type: integer
        format: int16
      workEnd:
        type: integer
        format: int16
      workDays:
        type: array
        items:
          type: integer
          format: int16
      roles:
        type: array
        items:
          type: string
      groups:
        type: array
        items:
          type: integer
          format: int32
      registeredStamp:
        type: integer
        format: int64
  Domain:
    required:
      - email
    properties:
      email:
        type: string
      country:
        type: string
      state:
        type: string
      city:
        type: string
      address:
        type: string
      zipcode:
        type: string
      primaryPhone:
        type: string
      locale:
        type: string
      timezone:
        type: string
      registeredStamp:
        type: integer
        format: int64
  LinkedDomain:
    required:
    - 'domainKey'
    - 'apiKey'
    - 'token'
    - 'label'
    properties:
      domainKey:
        type: string
      apiKey:
        type: string
      token:
        type: string
      label:
        type: string
  UserDomain:
    required:
      - user
      - domain
      - token
      - domainKey
      - apiKey
    properties:
      user:
        $ref: "#/definitions/User"
      domain:
        $ref: "#/definitions/Domain"
      token:
        type: string
      domainKey:
        type: string
      apiKey:
        type: string
      label:
        type: string
      linkedDomains:
        type: array
        items:
          $ref: "#/definitions/LinkedDomain"
  ApiError: 
    type: "object"
    required: 
      - "code"
    properties: 
      code: 
        type: "string"
      message: 
        type: "string"
  Success: 
    type: "object"
    required:
      - code
    properties: 
      code: 
        type: "string"
      message: 
        type: "string"
  UserGroup:
    type: object
    required:
      - userId
      - id
      - name
    properties:
      userId:
        type: string
      id:
        type: integer
        format: int32
      name:
        type: string
  AssetGroup:
    type: object
    required:
      - assetId
      - id
      - name
    properties:
      assetId:
        type: string
      id:
        type: integer
        format: int32
      name:
        type: string
  DomainAssetGroup:
    type: object
    required:
      - id
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
  DeviceGroup:
    type: object
    required:
      - id
      - deviceId
      - name
    properties:
      id:
        type: integer
        format: int32
      deviceId:
        type: string
      name:
        type: string
  DomainDeviceGroup:
    type: object
    required:
      - id
      - name
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
  DomainUserGroup:
    type: object
    required:
      - id
      - name
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
      email:
        type: string
      primaryPhone:
        type: string
      locale:
        type: string
      timezone:
        type: string
      workStart:
        type: integer
        format: int16
      workEnd:
        type: integer
        format: int16
      workDays:
        type: array
        items:
          type: integer
          format: int16
  DeviceModel:
    type: object
    required:
      - id
      - version
    properties:
      id:
        type: string
      version:
        type: string
      description:
        type: string
      registeredStamp:
        type: integer
        format: int64
  Firmware:
    type: object
    required:
      - deviceModel
      - version
    properties:
      deviceModel:
        type: string
      version:
        type: string
      description:
        type: string
      fileName:
        type: string
      contentType:
        type: string
      createAt:
        type: integer
        format: int64
  Device:
    type: object
    discriminator: deviceType
    required:
      - id
      - modelId
      - version
    properties:
      id:
        type: string
      modelId:
        type: string
      version:
        type: string
      name:
        type: string
      description:
        type: string
      registeredStamp:
        type: integer
        format: int64
      password:
        type: string
      assetId:
        type: string
  FCMDevice:
    allOf:
    - $ref: '#/definitions/Device'
    - type: object
      required:
       - 'fcmToken'
      properties:
        fcmToken:
          type: string
  Asset:
    type: object
    required:
      - id
    properties:
      id:
        type: string
      name:
        type: string
      description:
        type: string
      registeredStamp:
        type: integer
        format: int64
  Template:
    type: object
    required:
    - name
    - lang
    - code
    properties:
      name:
        type: string
      lang:
        type: string
        enum:
          - JTWIG
      code:
        type: string
  Rule:
    type: object
    discriminator: ruleType
    required:
    - lang
    - code
    properties:
      lang:
        type: string
        enum:
          - GROOVY
          - JS
      code:
        type: string
  DomainRule:
    allOf:
    - $ref: '#/definitions/Rule'
    - type: object
  MessageRule:
    allOf:
    - $ref: '#/definitions/Rule'
    - type: object
      required:
       - 'messageId'
      properties:
        messageId:
          type: integer
          format: int32
        messageName:
          type: string
  ScheduledRule:
    allOf:
    - $ref: '#/definitions/Rule'
    - type: object
      required:
       - 'id'
       - 'pattern'
      properties:
        id:
          type: integer
          format: int32
        pattern:
          type: string
  NamedRule:
    allOf:
    - $ref: '#/definitions/Rule'
    - type: object
      required:
       - 'name'
      properties:
        name:
          type: string
  PushResult:
    type: object
    required:
      - muid
    properties:
      muid:
        type: string
        description: message unique identifier
  MessageHeader:
    type: object
    properties:
      key:
        type: string
        description: domain key
      api:
        type: string
        description: domain api-key
      did:
        type: string
        description: device id
      dmdl:
        type: string
        description: device model
      fwver:
        type: string
        description: device firmware version
      mid:
        type: integer
        format: int32
        description: message identifier
    required:
      - key
      - api
      - did
      - dmdl
      - fwver
      - mid
  PushMessage:
    type: object
    properties:
      header:
        $ref: '#/definitions/MessageHeader'
      data:
        type: string
        description: message payload JSON object
    required:
      - header
      - data
  DeviceMessage:
    type: object
    properties:
      header:
        type: string
        description: message header JSON object
      data:
        type: string
        description: message payload JSON object
      receivedStamp:
        type: integer
        format: int64
      id:
        type: string
        format: uuid
      messageId:
        type: integer
        format: int32
      nodeId:
        type: string
      nodeUUID:
        type: string
        format: uuid
      deviceModel:
        type: string
      firmwareVersion:
        type: string
      ipAddress:
        type: string
      port:
        type: integer
        format: int32
      channel:
        type: string
        enum:
          - HTTP
          - MQTT
          - UDP
          - TCP
          - COAP
          - FCM
    required:
      - header
      - data
  Command:
    type: object
    properties:
      commandId:
        type: integer
        format: int32
        description: message id
      data:
        type: string
        description: command payload JSON object
    required:
      - commandId
      - data
  BroadcastCommand:
    type: object
    properties:
      deviceIds:
        type: array
        items:
          type: string
      command:
        $ref: '#/definitions/Command'
    required:
      - command
  BroadcastData:
    type: object
    properties:
      deviceIds:
        type: array
        items:
          type: string
      data:
        type: string
    required:
      - data
  CommandStatus:
    type: object
    required:
      - corrId
      - status
    properties:
      corrId:
        type: integer
        format: int64
        description: unique command identifier or this device
      status:
        type: string
        enum:
          - QUEUED
          - NOT_CONNECTED
          - SENT
          - FAILED
          - ACKED
          - NACKED
      createdStamp:
        type: integer
        format: int64
      queuedStamp:
        type: integer
        format: int64
      sentStamp:
        type: integer
        format: int64
      ackedStamp:
        type: integer
        format: int64
      commandType:
        type: string
        enum:
          - JSON_COMMAND
          - RAW_COMMAND
      dataChannel:
        type: string
        enum:
          - UDP
          - MQTT
          - HTTP
          - COAP
          - TCP
          - FCM
      nodeId:
        type: string
      nodeUid:
        type: string
        format: uuid
      description:
        type: string
      reportedIp:
        type: string
      reportedPort:
        type: integer
        format: int32
  Storable:
    type: object
    discriminator: storableType
    properties:
      dataType:
        type: string
        enum:
          - VARCHAR
          - TEXT
          - BOOLEAN
          - BIGINT
          - BLOB
          - DOUBLE
          - FLOAT
          - INTEGER
          - DATE
          - TIMESTAMP
          - UUID
      format:
        type: string
        description: Required if **dataType** is **blob**
        enum:
          - AS_IS
          - JSON
          - HEX
          - BASE64
      label:
        type: string
      description:
        type: string
  Property:
    allOf:
    - $ref: '#/definitions/Storable'
    - type: object
      discriminator: propertyType
      required:
      - 'name'
      - 'value'
      properties:
        name:
          type: string
        value:
          type: string
  DeviceProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'deviceId'
      properties:
        deviceId:
          type: string
  DeviceModelProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'deviceModelId'
      properties:
        deviceModelId:
          type: string
  DeviceGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'deviceId'
      - 'deviceGroupId'
      properties:
        deviceId:
          type: string
        deviceGroupId:
          type: integer
          format: int32
  AssetProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'assetId'
      properties:
        assetId:
          type: string
  AssetGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'assetId'
      - 'assetGroupId'
      properties:
        assetId:
          type: string
        assetGroupId:
          type: integer
          format: int32
  UserProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'userId'
      properties:
        userId:
          type: string
  UserGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'userId'
      - 'userGroupId'
      properties:
        userId:
          type: string
        userGroupId:
          type: integer
          format: int32
  DomainProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
  DomainDeviceGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'groupId'
      properties:
        groupId:
          type: integer
          format: int32
  DomainUserGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'groupId'
      properties:
        groupId:
          type: integer
          format: int32
  DomainAssetGroupProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'groupId'
      properties:
        groupId:
          type: integer
          format: int32
  GeofenceProperty:
    allOf:
    - $ref: '#/definitions/Property'
    - type: object
      required:
      - 'geofenceName'
      properties:
        geofenceName:
          type: string
  MessageField:
    allOf:
    - $ref: '#/definitions/Storable'
    - type: object
      required:
       - 'name'
      properties:
        name:
          type: string
  MessageSpecification:
    type: object
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
      label:
        type: string
      description:
        type: string
      fields:
        type: array
        items:
          $ref: '#/definitions/MessageField'
    required:
      - id
      - name
      - fields
  CommandField:
    allOf:
    - $ref: '#/definitions/Storable'
    - type: object
      required:
       - 'commandId'
       - 'name'
      properties:
        commandId:
          type: integer
          format: int32
        name:
          type: string
        value:
          type: string
  CommandDefinition:
    type: object
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
      description:
        type: string
      fields:
        type: array
        items:
          $ref: '#/definitions/CommandField'
    required:
      - id
      - name
      - fields
  RecordField:
    allOf:
    - $ref: '#/definitions/Storable'
    - type: object
      required:
       - 'name'
      properties:
        name:
          type: string
  RecordDefinition:
    type: object
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
      description:
        type: string
      fields:
        type: array
        items:
          $ref: '#/definitions/RecordField'
    required:
      - id
      - name
      - fields
  Param: 
    type: 'object'
    required: 
      - 'name'
    properties: 
      name: 
        type: 'string'
      value: 
        type: 'string'
  SearchQuery:
    type: object
    discriminator: searchQueryType
    required:
      - 'method'
    properties:
      method:
        type: string
        enum:
          - GET
          - POST
          - HEAD
          - PUT
          - DELETE
          - OPTIONS
          - TRACE
      extraPath:
        type: string
      query:
        type: string
        description: ElasticSearch Query String
      params:
        type: array
        items:
          $ref: '#/definitions/Param'
  TemplateQuery:
    type: object
    discriminator: templateQueryType
    required:
      - 'method'
      - 'templateName'
    properties:
      method:
        type: string
        enum:
          - GET
          - POST
          - HEAD
          - PUT
          - DELETE
          - OPTIONS
          - TRACE
      systemTemplate:
        type: boolean
      templateName:
        type: string
      mergeContent:
        type: string
        description: Merging JSON string
      extraPath:
        type: string
      params:
        type: array
        items:
          $ref: '#/definitions/Param'
  SearchResult:
    type: object
    required:
      - httpCode
    properties:
      httpCode:
        type: integer
        format: int32
      result:
        type: string
  Event:
    type: object
    required:
      - id
      - name
    properties:
      id:
        type: integer
        format: int32
      name:
        type: string
      subject:
        type: string
      content:
        type: string
  EventRegistration:
    type: object
    required:
      - id
      - channel
      - address
    properties:
      id:
        type: integer
        format: int32
      channel:
        type: string
        enum:
          - 'EMAIL'
          - 'SMS'
          - 'VOICE'
          - 'FCM'
      address:
        type: string
  ScriptResult:
    type: object
    required:
      - session
    properties:
      session:
        type: string
        format: uuid
  Geofence:
    type: object
    required:
      - name
      - geoType
    properties:
      name:
        type: string
      geoType:
        type: string
      label:
        type: string
      description:
        type: string
      coordinates:
        type: string
      radius:
        type: string
      geometries:
        type: string
      createdAt:
        type: integer
        format: int64
  Location:
    type: object
    required:
      - assetId
    properties:
      assetId:
        type: string
      at:
        type: string
        format: uuid
      lat:
        type: number
        format: double
      lon:
        type: number
        format: double
      deviceId:
        type: string
  Count:
    type: object
    required:
      - total
    properties:
      total:
        type: integer
        format: int64
  OTABatch:
    type: object
    discriminator: otaBatchType
    required:
      - id
      - state
      - toModel
      - toVersion
      - createdAt
      - expireAt
    properties:
      id:
        type: string
        format: uuid
      state:
        type: string
        enum:
          - CREATING
          - CREATED
          - RUNNING
          - CANCELLED
          - FAILED
          - PARTIAL_COMPLETE
          - COMPLETE        
      name:
        type: string
      message:
        type: string
      toModel:
        type: string
      toVersion:
        type: string
      createdAt:
        type: integer
        format: int64
      expireAt:
        type: integer
        format: int64
      finishedAt:
        type: integer
        format: int64
  OTAModelBatch:
    allOf:
    - $ref: '#/definitions/OTABatch'
    - type: object
    required:
      - fromModel
      - fromVersion
    properties:
      fromModel:
        type: string
      fromVersion:
        type: string
  OTADeviceBatch:
    allOf:
    - $ref: '#/definitions/OTABatch'
    - type: object
  OTABatchMember:
    type: object
    discriminator: otaBatchMemberType
    required:
      - id
      - deviceId
      - state
    properties:
      id:
        type: string
        format: uuid
      deviceId:
        type: string
      state:
        type: string
        enum:
          - PENDING
          - DOWNLOADING
          - FAILED
          - SUCCESS
      beginStamp:
        type: integer
        format: int64
      endStamp:
        type: integer
        format: int64
      failures:
        type: integer
        format: int32
  OTAModelBatchMember:
    allOf:
    - $ref: '#/definitions/OTABatchMember'
    - type: object
  OTADeviceBatchMember:
    allOf:
    - $ref: '#/definitions/OTABatchMember'
    - type: object
  FirmwareInfo:
    type: object
    required:
      - model
      - version
    properties:
      model:
        type: string
      version:
        type: string
  Script:
    type: object
    required:
      - sessionId
      - code
    properties:
      sessionId:
        type: string
        format: uuid
      code:
        type: string
  PropertyCommand:
    type: object
    required:
      - 'target'
    properties:
      target:
        type: string
        enum:
          - ASSET
          - ASSET_GROUP
          - DEVICE
          - DEVICE_GROUP
          - DEVICE_MODEL
          - DOMAIN
          - DOMAIN_ASSET_GROUP
          - DOMAIN_DEVICE_GROUP
          - DOMAIN_USER_GROUP
          - GEOFENCE
          - USER
          - USER_GROUP
      assetId:
        type: string
      deviceId:
        type: string
      deviceModelId:
        type: string
      userId:
        type: string
      geofenceId:
        type: string
      groupId:
        type: integer
        format: int32
  Lookup:
    type: object
    required:
      - 'target'
      - 'dataType'
      - 'name'
      - 'value'
    properties:
      target:
        type: string
        enum:
          - ASSET
          - ASSET_GROUP
          - DEVICE
          - DEVICE_GROUP
          - DEVICE_MODEL
          - DOMAIN
          - DOMAIN_ASSET_GROUP
          - DOMAIN_DEVICE_GROUP
          - DOMAIN_USER_GROUP
          - GEOFENCE
          - USER
          - USER_GROUP
      dataType:
        type: string
        enum:
          - BOOLEAN
          - BYTE
          - CHAR
          - SHORT
          - INT
          - LONG
          - FLOAT
          - DOUBLE
          - STRING
          - UUID
          - BLOB
      name:
        type: string
      value:
        type: string
      assetId:
        type: string
      deviceId:
        type: string
      deviceModelId:
        type: string
      userId:
        type: string
      geofenceId:
        type: string
      groupId:
        type: integer
        format: int32
  LookupGet:
    type: object
    required:
      - 'target'
      - 'name'
    properties:
      target:
        type: string
        enum:
          - ASSET
          - ASSET_GROUP
          - DEVICE
          - DEVICE_GROUP
          - DEVICE_MODEL
          - DOMAIN
          - DOMAIN_ASSET_GROUP
          - DOMAIN_DEVICE_GROUP
          - DOMAIN_USER_GROUP
          - GEOFENCE
          - USER
          - USER_GROUP
      name:
        type: string
      assetId:
        type: string
      deviceId:
        type: string
      deviceModelId:
        type: string
      userId:
        type: string
      geofenceId:
        type: string
      groupId:
        type: integer
        format: int32
  LookupResult:
    type: object
    required:
      - 'dataType'
      - 'name'
      - 'value'
    properties:
      dataType:
        type: string
        enum:
          - BOOLEAN
          - BYTE
          - CHAR
          - SHORT
          - INT
          - LONG
          - FLOAT
          - DOUBLE
          - STRING
          - UUID
          - BLOB
      name:
        type: string
      value:
        type: string
  LookupDelete:
    type: object
    required:
      - 'target'
      - 'name'
    properties:
      target:
        type: string
        enum:
          - ASSET
          - ASSET_GROUP
          - DEVICE
          - DEVICE_GROUP
          - DEVICE_MODEL
          - DOMAIN
          - DOMAIN_ASSET_GROUP
          - DOMAIN_DEVICE_GROUP
          - DOMAIN_USER_GROUP
          - GEOFENCE
          - USER
          - USER_GROUP
      name:
        type: string
      assetId:
        type: string
      deviceId:
        type: string
      deviceModelId:
        type: string
      userId:
        type: string
      geofenceId:
        type: string
      groupId:
        type: integer
        format: int32
  NodeProperty:
    type: object
    required:
      - 'name'
      - 'value'
    properties:
      name:
        type: string
      value:
        type: string
  ClusterNode:
    type: object
    properties:
      nodeId:
        type: string
      nodeUid:
        type: string
      consistentId:
        type: string
      addresses:
        type: array
        items:
          type: string
      hostNames:
        type: array
        items:
          type: string
      props:
        type: array
        items:
          $ref: '#/definitions/NodeProperty'
  EmailGateway:
    type: object
    required:
      - 'host'
      - 'port'
      - 'user'
      - 'password'
      - 'primaryEmail'
    properties:
      host:
        type: string
      port:
        type: integer
        format: int32
      user:
        type: string
      password:
        type: string
      primaryEmail:
        type: string
      bounceEmail:
        type: string
      ssl:
        type: boolean
      tls:
        type: boolean
      debug:
        type: boolean
  TwilioGateway:
    type: object
    required:
      - 'sid'
      - 'token'
      - 'primaryPhone'
    properties:
      sid:
        type: string
      token:
        type: string
      primaryPhone:
        type: string
      debug:
        type: boolean
  FCMGateway:
    type: object
    required:
      - 'apiKey'
    properties:
      apiKey:
        type: string
      debug:
        type: boolean
  UDPGateway:
    type: object
    required:
      - 'port'
      - 'threads'
    properties:
      port:
        type: integer
        format: int32
      threads:
        type: integer
        format: int32
      decoderCode:
        type: string
  GoogleMaps:
    type: object
    required:
      - 'apiKey'
    properties:
      apiKey:
        type: string
